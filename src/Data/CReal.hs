{-# Language BangPatterns #-}

-- | A hopefully correct implentation of Computable Reals
--
-- Copyright Ludvik 'Probie' Galois 2015

module Data.CReal (CReal, approximate, (~=), (=~)) where

import Data.Ratio

infix 5 ~=
infix 4 =~

-- A computation is a function which takes an accuracy and returns a
-- Rational
                
type Computation = Integer -> Rational

-- | An opaque type for representing computable reals
newtype CReal = CReal Computation

-- For easy debugging, we define Show by calculating to more precision
-- than a Double, and then showing as a Double

instance Show CReal where
  show x = show (fromRational (approximate x 20) :: Double)
  
-- Note that equality can diverge
instance Eq CReal where
  x == y = eqr x y 1
    where eqr x y n = approximate x n == approximate y n && eqr x y (n+1)
  
-- Ordering can also diverge
instance Ord CReal where
  compare x y = compr x y 1
    where compr x y n = case approximate x n `compare` approximate y n of
            LT -> LT
            GT -> GT
            EQ -> compr x y (n+1)

instance Num CReal where
  fromInteger x = CReal (const (x%1))
  (CReal x) + (CReal y) = CReal (add x y)
  (CReal x) * (CReal y) = CReal (mult x y)
  abs (CReal x) = CReal (abs . x)
  (CReal x) - (CReal y) = CReal (sub x y)
  signum (CReal x) = CReal (signumc x)
  
data ApproxEq = ApproxEq CReal Integer
  
-- | Approximately equal, used with (=~) as an abuse of notation so we
-- can write
-- @
-- x ~=n=~ y
-- @
-- which will check if
-- @
-- (approximate x n) == (approximate y n)
-- @
(~=) :: CReal -> Integer -> ApproxEq
x ~= delta = ApproxEq x delta

-- | See (~=)
(=~) :: ApproxEq -> CReal -> Bool
(ApproxEq x delta) =~ y = approximate x delta == approximate y delta


-- | Convert a CReal to a rational
approximate :: CReal -> Integer -> Rational
approximate (CReal f) n' = (((x - 1) % n) + ((x + 1) % n)) * (1 % 2)
  where x = round (f n * (n%1))
        n = 10^(n' - 1)

add :: Computation -> Computation -> Computation
add x y n = x n + y n

mult :: Computation -> Computation -> Computation
mult x y n = x n * y n

sub :: Computation -> Computation -> Computation
sub x y n = x n - y n

signumc :: Computation -> Computation
signumc x n = signum (x n)

instance Fractional CReal where
  (CReal x) / (CReal y) = CReal (divide x y)
  fromRational x = CReal (const x)

divide :: Computation -> Computation -> Computation
divide x y n = x n / y n

instance Floating CReal where
  pi = 4 * ((4 * atan (fromRational (1%5))) -
            atan (fromRational (1%239)))
  exp (CReal x) = CReal (expc x)
  atan (CReal x) = CReal (atanc x)
  atanh (CReal x) = CReal (atanhc x)
  log (CReal x) = CReal (logc x)
  sin (CReal x) = CReal (sinc x)
  cos (CReal x) = CReal (cosc x)
  asin (CReal x) = CReal (asinc x)
  asinh z = log (z + sqrt(z*z + 1))
  acos x = pi / 2 - asin x
  acosh z = log (z + sqrt (z*z + 1) * sqrt (z*z-1))
  sinh x = (exp x - exp (-x)) / 2
  cosh x = (exp x + exp (-x)) / 2

-- This seemed pretty useful since we deal with a lot of power series
sigma :: Rational -> Integer -> Rational -> (Integer -> Rational) -> Rational
sigma prec !n !acc f = if abs term < (prec / 2)
                       then term+acc
                       else sigma prec (n+1) (term+acc) f
  where term = f n

atanc :: Computation -> Computation
atanc z digits = sigma acc 0 0 (\n -> (((-1)^n) * (z'^(2*n+1))) * (1%(2*n+1)))
    where acc = 1 % (digits * 10)
          z' = z digits

atanhc :: Computation -> Computation
atanhc z digits = sigma acc 0 0 (\n -> (z'^(2*n+1)) * (1%(2*n+1)))
    where acc = 1% (digits * 10)
          z' = z digits

expc :: Computation -> Computation
expc z digits = sigma acc 0 0 (\n -> (z'^n) * (1 % fac n))
    where acc = 1 % (digits * 10)
          z' = z digits

logc :: Computation -> Computation
logc z digits = logc' (digits*10) z (digits*10)
          
-- Our method for checking if we're accurate enough could use work
logc' :: Integer -> Computation -> Computation
logc' digits' z digits = if p then logc' (digits' * 10) z digits
                         else guess digits'
    where acc = 1 % (digits * 10)
          guess d' = 2 * atanhc (\d -> 
                             (z d - 1) / (z d + 1)) d'
          guessr = expc guess digits'
          upbound = expc (const acc) digits'
          lowbound = expc (const (negate acc)) digits'
          upbound' = round (upbound * recip (acc*10))
          lowbound' = round (lowbound * recip (acc*10))
          p = upbound' /= lowbound'

fac n = fac' n 1
  where fac' 0 !acc = acc
        fac' n !acc = fac' (n-1) (n*acc)

sinc :: Computation -> Computation
sinc z digits = sigma acc 0 0 (\n -> ((-1)^n)%fac (2*n+1)*(z'^(2*n+1)))
    where acc = 1 % (digits * 10)
          z' = z digits

cosc :: Computation -> Computation
cosc z digits = sigma acc 0 0 (\n -> ((-1)^n)%fac (2*n) * (z'^(2*n)))
    where acc = 1 % (digits * 10)
          z' = z digits

asinc :: Computation -> Computation
asinc z digits = sigma acc 0 0
                 (\n -> (binom (2*n) n * (1%(4^n*(2*n+1)))) * (z'^(2*n+1)))
    where acc = 1 % (digits * 10)
          z' = z digits

binom :: Integer -> Integer -> Rational
binom n k = fac n % (fac k * fac (n - k))


 
